import networkx as nx
from src.app import util
import graph
import roomfortest
graphs = {'A': {'B', 'C'},
         'B': {'A', 'D', 'E'},
         'C': {'A', 'F'},
         'D': {'B'},
         'E': {'B', 'F'},
         'F': {'C', 'E'}}
# ['A', 'C', 'F']
def test_check_shortest_path():
    my_path = roomfortest.shortest_path(graphs, 'A', 'F')

    assert my_path == ['A', 'C', 'F']

# def test_check_shortest_paths():
#     # load data
#     # graph_data = graph.graph_loader('data.txt')
#     graph_data = graphs
#
#     # create graph
#     nx_graph = graph.get_nx_graph(graph_data)
#     # my_graph = roomfortest.shortest_path(graph_data, 'A', 'D')
#
#     # get shortest_path
#     expected_path = nx.shortest_path(nx_graph, source='A', target='F')
#
#     assert 2 == expected_path